License

Requantize MPEG-2 streams without recompressing.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Introduction

I recently discovered that 'tcrequant' (part of the transcode suite of tools)
has been deprecated. Worst still I found that when I ran 'tcrequant' on my 64-bit 
Linux workstation is was corrupting the video. See the links below for details.

 - http://tcforge.berlios.de/archives/2009/01/18/transcode_1_1_0_final_release/index.html
 - http://www.mail-archive.com/transcode-users@exit1.org/msg01773.html
 
Therefore I decided to get the M2VRequantiser code from Metakine working on both
32-bit and 64-bit Linux as a replacement for 'tcrequant'.

I've tried to preserve the credits and copyrights where they are known. I forked 
M2VRequantiser from a source tarball downloaded on April 20th 2009 from the URL
below.

 - http://www.metakine.com/products/dvdremaster/developers.html.

These credits/copyrights are taken from the source tarball I downloaded.

 * M2VRequantiser is based on libmpeg2 and mpeg2enc which are released under the 
   GNU General Public License and copyright by their respective owners.
 * M2VRequantiser by Makira
 * New code and modifications copyright Antoine Missout
 * Thanks to Sven Goethel for error resilience patches
 
Usage

M2VRequantiser accepts the raw MPEG2 video data (not VOB) from the standard 
input and writes the recompressed frames to the standard output. 

M2VRequantiser takes two arguments. The first one is a floating point value 
specifying the ratio of compression. The second is the size of the M2V, since 
the data is streamed to M2VRequantiser it cannot know the M2V size. The 
following command would recompress 'original.m2v', whose size is 1024000 bytes, 
by a factor of 1.25.

  M2VRequantiser 1.25 1024000 < original.m2v > requantised.m2v

Install

To compile M2VRequantiser run 'make'.
To install M2VRequantiser run 'make install' as root.

Known Limitations

 - Only tested on 32-bit and 64-bit Ubuntu.
 - I am not a C programmer ;-)

Source Code

You can grab the source from Launchpad. Contributions are welcome :-)

  - https://code.launchpad.net/m2vrequantiser

References

 - http://www.metakine.com/products/dvdremaster/developers.html
 - http://lxdvdrip.berlios.de/
   
v1.1 2010, 23rd July. 
 
 - Incorporated Debian packaging and patches from Christian Marillat
 - Now builds for all arches (Works for armel, mipsel and sparc).
 - Hosted project on Launchpad.
 
v1.0 2009, 23rd April.

 - Ported Stefan Becker's 64-bit compatibility and argument parsing patches
 - Added simple progress indicator and Makefile
