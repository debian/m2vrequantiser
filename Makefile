PREFIX    ?= /usr/local
CC         = gcc
CFLAGS    ?= -O2 -g -funroll-loops -frename-registers
CFLAGS    += -D_LARGEFILE_SOURCE -DLARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -D_REENTRANT
CFLAGS    += -Wall
OBJECTS    = main.o
LDFLAGS   += -s
LOADLIBES += -lm
INSTALL   ?= install

all:	M2VRequantiser

M2VRequantiser : $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LDFLAGS) $(LOADLIBES) -o M2VRequantiser

%.o : %.c
	$(CC) $(CFLAGS) -c $<

install:	all
	$(INSTALL) -d $(PREFIX)/bin
	$(INSTALL) -m 755 M2VRequantiser $(PREFIX)/bin
	
uninstall:
	rm -f $(PREFIX)/bin/M2VRequantiser

clean:
	rm -f M2VRequantiser *.o *~ core
